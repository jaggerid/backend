-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2019 at 03:52 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `course_backend_db`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `get game data by status` (IN `pStatus` BOOLEAN)  NO SQL
BEGIN
	SELECT * FROM game_tbl WHERE status = pStatus;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_game001_leaderboard` ()  NO SQL
BEGIN
	SELECT * FROM game001_leaderboard;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `game_tbl`
--

CREATE TABLE `game_tbl` (
  `game_id` int(11) NOT NULL,
  `game_name` varchar(50) NOT NULL,
  `max_level` int(11) NOT NULL,
  `leaderboard_type` int(2) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `game_tbl`
--

INSERT INTO `game_tbl` (`game_id`, `game_name`, `max_level`, `leaderboard_type`, `status`) VALUES
(1, 'Game-001', 10, 1, 1),
(2, 'Game-002', 5, 1, 1),
(3, 'Game-003', 20, 2, 1),
(4, 'Game-004', 10, 2, 1),
(5, 'Game-005', 10, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `leaderboard_tbl`
--

CREATE TABLE `leaderboard_tbl` (
  `leaderboard_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `start_period` datetime NOT NULL,
  `end_period` datetime NOT NULL,
  `submit_date` datetime NOT NULL DEFAULT current_timestamp(),
  `level` int(11) NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `leaderboard_tbl`
--

INSERT INTO `leaderboard_tbl` (`leaderboard_id`, `user_id`, `game_id`, `start_period`, `end_period`, `submit_date`, `level`, `score`) VALUES
(1, 1, 1, '2019-12-01 00:00:00', '2019-12-31 00:00:00', '2019-12-19 15:49:52', 1, 100),
(2, 2, 1, '2019-12-01 00:00:00', '2019-12-31 00:00:00', '2019-12-19 17:00:09', 10, 95),
(3, 2, 1, '2019-12-01 00:00:00', '2019-12-31 00:00:00', '2019-12-19 17:01:37', 10, 195),
(4, 2, 1, '2019-12-01 00:00:00', '2019-12-31 00:00:00', '2019-12-19 17:01:37', 1, 1),
(5, 2, 1, '2019-12-01 00:00:00', '2019-12-31 00:00:00', '2019-12-19 17:01:37', 10, 500),
(6, 2, 1, '2019-12-01 00:00:00', '2019-12-31 00:00:00', '2019-12-19 17:01:37', 9, 55),
(7, 2, 1, '2019-12-01 00:00:00', '2019-12-31 00:00:00', '2019-12-19 17:01:37', 2, 15),
(8, 2, 2, '2019-12-01 00:00:00', '2019-12-31 00:00:00', '2019-12-19 17:02:11', 10, 195),
(9, 2, 3, '2019-12-01 00:00:00', '2019-12-31 00:00:00', '2019-12-19 17:02:11', 1, 1),
(10, 2, 3, '2019-12-01 00:00:00', '2019-12-31 00:00:00', '2019-12-19 17:02:12', 10, 500),
(11, 2, 4, '2019-12-01 00:00:00', '2019-12-31 00:00:00', '2019-12-19 17:02:12', 9, 55),
(12, 2, 5, '2019-12-01 00:00:00', '2019-12-31 00:00:00', '2019-12-19 17:02:12', 2, 15),
(15, 2, 4, '2019-12-01 00:00:00', '2019-12-31 00:00:00', '2019-12-19 21:45:07', 1, 111);

-- --------------------------------------------------------

--
-- Table structure for table `user_tbl`
--

CREATE TABLE `user_tbl` (
  `user_id` int(11) NOT NULL,
  `username` varchar(16) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(25) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `date_birth` date NOT NULL,
  `place_birth` varchar(50) NOT NULL,
  `address` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_tbl`
--

INSERT INTO `user_tbl` (`user_id`, `username`, `password`, `email`, `firstname`, `lastname`, `date_birth`, `place_birth`, `address`) VALUES
(1, 'testuser', 'testuser', 'testuser@test.com', 'test', 'user', '0000-00-00', 'bandung', 'bandung, jawa bara'),
(2, 'testuser2', '58dd024d49e1d1b83a5d307f09f32734', 'testuser2@test2.com', 'test', 'user2', '2019-12-08', 'Tangerang', 'Tangerang, banten'),
(3, 'testuser3', '1e4332f65a7a921075fbfb92c7c60cce', 'testuser3@test.com', 'test', 'user3', '2019-12-10', 'jakarta', 'jakarta barat');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `game_tbl`
--
ALTER TABLE `game_tbl`
  ADD PRIMARY KEY (`game_id`);

--
-- Indexes for table `leaderboard_tbl`
--
ALTER TABLE `leaderboard_tbl`
  ADD PRIMARY KEY (`leaderboard_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `game_id` (`game_id`);

--
-- Indexes for table `user_tbl`
--
ALTER TABLE `user_tbl`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `game_tbl`
--
ALTER TABLE `game_tbl`
  MODIFY `game_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `leaderboard_tbl`
--
ALTER TABLE `leaderboard_tbl`
  MODIFY `leaderboard_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user_tbl`
--
ALTER TABLE `user_tbl`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `leaderboard_tbl`
--
ALTER TABLE `leaderboard_tbl`
  ADD CONSTRAINT `FK_GAME_ID` FOREIGN KEY (`game_id`) REFERENCES `game_tbl` (`game_id`),
  ADD CONSTRAINT `FK_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `user_tbl` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
