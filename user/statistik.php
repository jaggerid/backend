<?php

SESSION_START();

include("../database.php"); // sertakan database.php untuk dapat menggunakan class database
$db = new Database(); // membuat objek baru dari class database agar dapat menggunakan fungsi didalamnya
$username = (isset($_SESSION['username'])) ? $_SESSION['username'] : "";

function get_options($select)
{
	$options='';
	for($x = 1; $x <= $i['max_level']; $x++)
	{
		
			$options='<option value="'.$x.'">'.$x.'</option>';
		
	}
	return $options;
}
			
if($username)
{
   $result = $db->execute("SELECT * FROM user_tbl WHERE username = '".$username."'");
   if(!$result)
   {
       // redirect ke halaman login, data tidak valid
       header("Location: http://localhost/couse_backend_assignment/");
   }
   // abaikan jika token valid
		   $statisticdata = $db->get("SELECT game_tbl.game_id as gameid, game_tbl.game_name as gamename, MIN(leaderboard_tbl.score) as min, MAX(leaderboard_tbl.score) as max, AVG(leaderboard_tbl.score) as avg 
									FROM leaderboard_tbl, game_tbl
									WHERE leaderboard_tbl.game_id = game_tbl.game_id AND leaderboard_tbl.user_id = (select user_tbl.user_id from user_tbl where user_tbl.username = '".$username."')
									group by game_tbl.game_id");               
}
else
{
   header("Location: http://localhost/couse_backend_assignment/");
}

$notification = (isset($_SESSION['notification'])) ? $_SESSION['notification'] : "";
if($notification)
{
   echo $notification;
   unset($_SESSION['notification']);   
}

?>

PAGE : STATISTIK

<table border=1>
   <tr>
	   <td>MENU</td>
       <td><a href="http://localhost/couse_backend_assignment/user/">HOME</a></td>
       <td><a href="http://localhost/couse_backend_assignment/user/statistik.php">STATISTIK</a></td>   
<td><a href="http://localhost/couse_backend_assignment/user/submit.php">SUBMIT SCORE</a></td>   	   
       <td><a href="http://localhost/couse_backend_assignment/user/leaderboard.php">LEADERBOARD</a></td>
       <td><a href="http://localhost/couse_backend_assignment/user/logout.php">LOGOUT</a></td>
   </tr>
</table>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>" METHOD="POST">
<table border=1>
   <tr><td align="center" colspan=5>USER STATISTIK SKOR GAME</td></tr>
   <tr><td>GAME</td><td>MIN</td><td>MAX</td><td>AVG</td><td>LEVEL</td></tr>
   <?php
   if($statisticdata)
   {
       while($row = mysqli_fetch_assoc($statisticdata))
       {
		   $selected='1';

			$r = $db->get("SELECT max_level FROM game_tbl where game_name = '".$row['gamename']."'");
			$i = mysqli_fetch_assoc($r);
			
			 
			 if(isset($_POST['level']))
			 {
			  $selected= $_POST['level'];
			  echo $selected;
			 }
           ?>
           <tr>
			    <td><?php echo $row['gamename']?></td>
			    <td><?php echo $row['min']?></td>
			    <td><?php echo $row['max']?></td>
			    <td><?php echo $row['avg']?></td>     
				<td>
					
					<select name="level" required>
					
					<?php echo get_options($selected); ?>
					</select>
				</td>			   
           </tr>

           <?php
       }
   }
   
   
   ?>

</table>
</form>

